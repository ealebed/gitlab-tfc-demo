policy "restrict-gce-machine-type-v3" {
    source = "./policies/restrict-gce-machine-type-v3.sentinel"
#    enforcement_level = "hard-mandatory" # policy must pass no matter what. The only way to override a hard mandatory policy is to explicitly remove the policy
#    enforcement_level = "soft-mandatory" # policy must pass unless an override is specified. The purpose of this level is to provide a level of privilege separation for a behavior
    enforcement_level = "advisory" # policy is allowed to fail. However, a warning should be shown to the user or logged
}
