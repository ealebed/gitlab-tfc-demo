variable "machine_type" {
    description = "GCP machine type"
    type = string
    default = "f1-micro"
}

variable "instance_name" {
    description = "GCP instance name"
    type = string
    default = "demo"
}

variable "image" {
    description = "GCP image"
    type = string
    default = "debian-cloud/debian-9"
}

variable "network_name" {
    description = "GCP network name"
    type = string
    default = "demo-network"
}

variable "varSet" {}
