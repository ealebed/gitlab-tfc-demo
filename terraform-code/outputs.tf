# output "network_name" {
#   value       = module.test-vpc-module.network_name
#   description = "The name of the VPC being created"
# }

# output "bucket" {
#   description = "The created storage bucket"
#   value       = module.storage.bucket_name
# }

output "show_gcp_project" {
  value = local.local_data.gcp_project
}

output "show_local_data" {
  value = local.local_data
}
