provider "google" {
    project = local.local_data.gcp_project
    region  = "europe-central2"
    zone    = "europe-central2-a"
}

locals {
  local_data = jsondecode(var.varSet)
}

# module "test-vpc-module" {
#   source       = "terraform-google-modules/network/google"
#   project_id   = var.gcp_project
#   network_name = var.network_name
#   mtu          = 1460
  
#   subnets      = [
#     {
#       subnet_name   = "subnet-01"
#       subnet_ip     = "10.10.10.0/24"
#       subnet_region = "europe-central2"
#     # },
#     # {
#     #     subnet_name           = "subnet-02"
#     #     subnet_ip             = "10.10.20.0/24"
#     #     subnet_region         = "us-west1"
#     #     subnet_private_access = true
#     #     subnet_flow_logs      = true
#     }
#   ]
# }

module "instance" {
  source        = "./modules/instance"

  gcp_project   = local.local_data.gcp_project
  instance_name = var.instance_name
  machine_type  = var.machine_type
  image         = var.image
  # network_name    = module.test-vpc-module.network_name
  # subnetwork_name = "subnet-01"
}

# module "storage" {
#   source        = "./modules/storage"

#   gcp_project   = local.local_data.gcp_project
# }
