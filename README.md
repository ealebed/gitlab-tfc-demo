## Description GitLab CI/CD pipeline for Terraform code

In general, GitLab CI/CD pipeline for applying terraform resources looks like:

![Pipeline](./images/tf-pipeline.png)

By default the GitLab CI/CD configuration file is `.gitlab-ci.yml` in the repository root folder. This file is self documented and well commented.

The `variables` section store information, that can be used in all job scripts in the pipeline. For security reasons, sensitive information (like passwords and secret keys) should be added on **Settings > CI/CD** in your project (or Group) and masked (hidden in job logs). Adding sensitive information to custom GitLab runner also can be an option.

The `default` section contains custom default values for job keywords (e. g. `tags` for selecting a specific runner from the list of all runners that are available for the project).

*OPTIONAL:* Inside the pipeline can be used YAML anchors - a feature which let identify an item and then reference it elsewhere in file. Anchors are created using the `&` sign. The sign is followed by an alias name. Usage aliases and anchors is aimed at reducing repeated blocks/similar configurations ("Don't repeat yourself" principle). Anchor declaration example:

```yaml
.terraform-before-script: &terraform-before-script
  - mkdir -p ${HOME}/.terraform.d
  - sed -e "s/my-tf-addr/${TFC_ADDR}/" -e "s/my-token/${TFC_TOKEN}/" < templates/credentials.tfrc.json.template > ${HOME}/.terraform.d/credentials.tfrc.json
  - sed -e "s/my-tf-addr/${TFC_ADDR}/" -e "s/my-org/${TFC_ORG}/" -e "s/my-tf-ws/${TFC_WORKSPACE}/" < templates/backend.tf.template > ${TF_CODE_DIR}/backend.tf
```

Anchor usage example:

```yaml
init:
  stage: init
  script:
    - *terraform-before-script
...
```

There are several stages in this CI/CD pipeline: `init`, `validate`, `plan`, `apply`, `destroy`. Stage `validate` has two steps, while all other stages has only one step.

- the `init` step used to prepare working directory and remote terraform backend for execution other commands;

- the `validate` step (in the same named stage) used to check whether the terraform configuration is valid;

- the `wiz-scan` step (in `validate` stage) used to perform WIZ.io policy checks on provided terraform code;

- the `plan` step needed to show changes required by the current terraform configuration;

- the `apply` step used to create (or update) infrastructure to achieve state described in terraform code. This step should be executed manually, from GitLab UI;

- the `destroy` step used to destroy previously created terraform infrastructure. This step should be executed manually, from GitLab UI - use this wisely.

Between `plan` and `apply` stages, HashiCorp Sentinel Policy check are performed on Terraform Enterprise's side. Results (whether the policy passed or failed) can be viewed in Terraform Enterprise's UI and/or in GitLab CI/CD pipeline's output. All Sentinel Policies configured with `enforcement_level = "advisory"` - it means, that policy is allowed to fail. However, a warning should be shown to the user or logged.
