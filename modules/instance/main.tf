resource "google_compute_instance" "demo" {
    name = var.instance_name
    machine_type = var.machine_type

    allow_stopping_for_update = true
    
    boot_disk {
        initialize_params {
            image = var.image
        }
    }

    network_interface {
        network = "default"
        # network    = var.network_name
        # subnetwork = var.subnetwork_name

        access_config {
            // Ephemeral IP
        }
    }
}
