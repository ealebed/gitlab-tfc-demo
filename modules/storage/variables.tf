variable "gcp_project" {
    description = "GCP project name"
}

/**
* OPTIONAL PARAMETERS
* These parameters have reasonable defaults.
*/

# variable "create_project_bucket" {
#   description = "Whether or not to create a GCS bucket for this project. If `'true'`, a logging bucket will automatically be created and logging will be enabled. If `configure_kms` is `'true`, any buckets created will be configured with encryption enabled using your project's KMS key."
#   default     = "true"
# }

variable "name_prefix" {
  description = "A prefix for your bucket names (not a storage prefix path)."
  default     = "demo"
}

variable "location" {
  description = "The location to create your project's storage resources in"
  default     = "US"
}

variable "storage_class" {
  description = "The storage class for your bucket."
  default     = "STANDARD"
}
