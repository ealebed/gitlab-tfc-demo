// Bucket for project sensitive object storage with versioning enabled
resource "google_storage_bucket" "store" {
  name          = "${var.gcp_project}-store"
  location      = var.location
  project       = var.gcp_project
  storage_class = var.storage_class

  versioning {
    enabled = "false"
  }

  // To prevent accidental destruction, you must manually destroy this bucket.
  # lifecycle {
  #   prevent_destroy = "true"
  # }
}
