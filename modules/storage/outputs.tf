output "bucket_name" {
  value       = google_storage_bucket.store.name
  description = "Bucket name (for single use)."
}
