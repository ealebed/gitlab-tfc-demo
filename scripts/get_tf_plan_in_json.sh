#!/bin/bash

# Check if curl and jq are installed
if ! [ -x "$(command -v jq)" ]; then
  echo "Error: jq is not installed. This script relies on curl and jq. Please install both and retry."
  exit 1
fi
if ! [ -x "$(command -v curl)" ]; then
  echo "Error: curl is not installed. This script relies on curl and jq. Please install both and retry."
  exit 1
fi

# Evaluate ${TFC_TOKEN} environment variable
# If not set, give error and exit
if [ ! -z "${TFC_TOKEN}" ]; then
  echo "TFC_TOKEN environment variable was found."
else
  echo "ERR: TFC_TOKEN environment variable was not set."
  echo "You must export/set the TFC_TOKEN environment variable."
  echo "It should be a user or team token that has write or admin"
  echo "permission on the workspace."
  echo "Exiting."
  exit 1
fi

# Evaluate ${TFC_ADDR} environment variable
# If not set, give error and exit
if [ ! -z "${TFC_ADDR}" ]; then
  echo "Using TFC host: ${TFC_ADDR}."
else
  echo "ERR: You must export/set the TFC_ADDR environment variable."
  echo "Exiting."
  exit 1
fi

# Parse COLORED gitlab job output and get run ID from string e.g.:
# https://app.terraform.io/app/ealebed/gitlab-tfc-demo/runs/run-H3yeQKZrREfr7vnX[0m
# Use `awk` to trim last part of path in URL and `sed` to remove foreground ANSI colors
run_id=$(grep 'run-' result.txt | awk -F"/" '{print $NF}' | sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2})?)?[mGK]//g" )
echo "Run ID is ${run_id}"

# Get plan ID from provided run
if [ ! -z "${run_id}" ]; then
  echo "Getting plan ID from provided run ${run_id}"
  curl \
    --silent \
    --header "Authorization: Bearer ${TFC_TOKEN}" \
    --header "Content-Type: application/vnd.api+json" \
    "https://${TFC_ADDR}/api/v2/runs/${run_id}" > runs.json

  plan_id=$(cat runs.json | jq -r .data.relationships.plan.data.id)
  echo "Plan ID is ${plan_id}"
else
  echo "Could not get Run ID, exitting..."
  exit
fi

# Get terraform plan in json and write to file:
if [ ! -z "${plan_id}" ]; then
  echo "Getting terraform plan in JSON format from provided plan ID ${plan_id}"
  curl \
    --silent \
    --location \
    --header "Authorization: Bearer ${TFC_TOKEN}" \
    --header "Content-Type: application/vnd.api+json" \
    "https://${TFC_ADDR}/api/v2/plans/${plan_id}/json-output" > plan.json
else
  echo "Could not get Plan ID, exitting..."
  exit
fi
