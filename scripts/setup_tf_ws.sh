#!/bin/bash

# Check if curl and jq are installed
if ! [ -x "$(command -v jq)" ]; then
  echo "Error: jq is not installed. This script relies on curl and jq. Please install both and retry."
  exit 1
fi
if ! [ -x "$(command -v curl)" ]; then
  echo "Error: curl is not installed. This script relies on curl and jq. Please install both and retry."
  exit 1
fi

# Evaluate ${TFC_TOKEN} environment variable
# If not set, give error and exit
if [ ! -z "${TFC_TOKEN}" ]; then
  echo "TFC_TOKEN environment variable was found."
else
  echo "ERR: TFC_TOKEN environment variable was not set."
  echo "You must export/set the TFC_TOKEN environment variable."
  echo "It should be a user or team token that has write or admin"
  echo "permission on the workspace."
  echo "Exiting."
  exit 1
fi

# Evaluate ${TFC_ADDR} environment variable
# If not set, give error and exit
if [ ! -z "${TFC_ADDR}" ]; then
  echo "Using TFC host: ${TFC_ADDR}."
else
  echo "ERR: You must export/set the TFC_ADDR environment variable."
  echo "Exiting."
  exit 1
fi

# Evaluate ${TFC_ORG} environment variable
# If not set, give error and exit
if [ ! -z "${TFC_ORG}" ]; then
  echo "Using TFC Organization: ${TFC_ORG}."
else
  echo "ERR: You must export/set the TFC_ORG environment variable."
  echo "Exiting."
  exit 1
fi

# Evaluate ${TFC_WORKSPACE} environment variable
# If not set, give error and exit
if [ ! -z "${TFC_WORKSPACE}" ]; then
  echo "Using TFC Workspace: ${TFC_WORKSPACE}."
else
  echo "ERR: You must export/set the TFC_WORKSPACE environment variable."
  echo "Exiting."
  exit 1
fi

# Evaluate that the ${TFC_WORKSPACE} name exist in the given organization
workspace_id=$(curl --silent --header "Authorization: Bearer ${TFC_TOKEN}" --header "Content-Type: application/vnd.api+json" "https://${TFC_ADDR}/api/v2/organizations/${TFC_ORG}/workspaces/${TFC_WORKSPACE}" | jq -r .data.id)
if [ -z "${workspace_id}" ] || [ "${workspace_id}" = null ]; then
  echo "WARN: Could not find workspace named: '${TFC_WORKSPACE}'."
  echo "Create workspace '${TFC_WORKSPACE}' from scratch."
  sed "s/my-tf-ws/${TFC_WORKSPACE}/" < templates/workspace.json.template > workspace.json
  curl \
    --silent \
    --header "Authorization: Bearer ${TFC_TOKEN}" \
    --header "Content-Type: application/vnd.api+json" \
    --request POST \
    --data @workspace.json \
    "https://${TFC_ADDR}/api/v2/organizations/${TFC_ORG}/workspaces" > workspace_creation_result.txt

  workspace_id=$(cat workspace_creation_result.txt | jq -r .data.id)
  echo "Workspace '${TFC_WORKSPACE}' created with ID ${workspace_id}"

  echo "Setting variables in '${TFC_WORKSPACE}' workspace..."
  # Terraform variable(s)
  sed -e "s/my-key/gcp_project/" -e "s/my-value/${GCP_PROJECT}/" -e "s/my-category/terraform/" -e "s/my-sensitive/false/" -e "s/my-workspace-id/${workspace_id}/" < templates/variable.json.template  > variable.json
  curl \
    --silent \
    --header "Authorization: Bearer ${TFC_TOKEN}" \
    --header "Content-Type: application/vnd.api+json" \
    --data @variable.json \
    "https://${TFC_ADDR}/api/v2/vars"

  # Environment variable(s)
  if [ ! -z "${GOOGLE_CREDENTIALS}" ]; then
    sed -e "s/my-key/GOOGLE_CREDENTIALS/" -e "s/my-value/${GOOGLE_CREDENTIALS}/" -e "s/my-category/env/" -e "s/my-sensitive/true/" -e "s/my-workspace-id/${workspace_id}/" < templates/variable.json.template  > variable.json
    curl \
      --silent \
      --header "Authorization: Bearer ${TFC_TOKEN}" \
      --header "Content-Type: application/vnd.api+json" \
      --data @variable.json \
      "https://${TFC_ADDR}/api/v2/vars"
  fi
else
  echo "Workspace '${TFC_WORKSPACE}' with ID ${workspace_id} already exists"
  # TODO: Do we need this?
  echo "Delete already existing variables..."
  curl \
    --silent \
    --header "Authorization: Bearer ${TFC_TOKEN}" \
    --header "Content-Type: application/vnd.api+json" \
    "https://${TFC_ADDR}/api/v2/vars?filter%5Borganization%5D%5Bname%5D=${TFC_ORG}&filter%5Bworkspace%5D%5Bname%5D=${TFC_WORKSPACE}" > vars.json

  x=$(cat vars.json | jq -r ".data[].id" | wc -l | awk '{print $1}')
  if [ ! -z "${x}" ] || [ ${x} -ne 0 ]; then
    for (( i=0; i<${x}; i++ )); do
      curl \
        --silent \
        --header "Authorization: Bearer ${TFC_TOKEN}" \
        --header "Content-Type: application/vnd.api+json" \
        --request DELETE \
        https://${TFC_ADDR}/api/v2/vars/$(cat vars.json | jq -r ".data[$i].id")
    done
  fi
  echo "Now we create variables from scratch"
  # Terraform variable
  sed -e "s/my-key/gcp_project/" -e "s/my-value/${GCP_PROJECT}/" -e "s/my-category/terraform/" -e "s/my-sensitive/false/" -e "s/my-workspace-id/${workspace_id}/" < templates/variable.json.template  > variable.json
  curl \
    --silent \
    --header "Authorization: Bearer ${TFC_TOKEN}" \
    --header "Content-Type: application/vnd.api+json" \
    --data @variable.json \
    "https://${TFC_ADDR}/api/v2/vars"

  # Environment variables
  if [ ! -z "${GOOGLE_CREDENTIALS}" ]; then
    sed -e "s/my-key/GOOGLE_CREDENTIALS/" -e "s/my-value/${GOOGLE_CREDENTIALS}/" -e "s/my-category/env/" -e "s/my-sensitive/true/" -e "s/my-workspace-id/${workspace_id}/" < templates/variable.json.template  > variable.json
    curl \
      --silent \
      --header "Authorization: Bearer ${TFC_TOKEN}" \
      --header "Content-Type: application/vnd.api+json" \
      --data @variable.json \
      "https://${TFC_ADDR}/api/v2/vars"
  fi  
fi
